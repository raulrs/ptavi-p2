# Protocolos para la transmisión de audio y video en Internet
# Práctica 2. Python avanzado

**Nota:** Esta práctica se puede entregar para su evaluación como parte de
la nota de prácticas, pudiendo obtener el estudiante hasta un punto. Para
las instrucciones de entrega, mira al final del documento. Para la evaluación
de esta entrega se valorará el correcto funcionamiento de lo que se pide y el
seguimiento de la guía de estilo de Python.

**Conocimientos previos necesarios:**

* Nociones de Python3 (las de la primera práctica).
* Nociones de git (las de la práctica de git)

**Tiempo estimado:** 6 horas

**Repositorio plantilla:** https://gitlab.etsit.urjc.es/ptavi/2021-2022/ptavi-p2

**Fecha de entrega parte individual:** 8 de octubre de 2021, 23:59 (hasta ejercicio 10, incluido)

**Fecha de entrega parte interoperación:** 11 de octubre de 2021, 23:59 (ejercicio 11)

## Introducción

La programación orientada a objetos en un paradigma de programación muy
utilizado en la actualidad y que conviene conocer para la realización de las
prácticas de la asignatura. En esta práctica se pretende explorar los conceptos
más importantes de este paradigma, implementando funcionalidad en Python.

## Objetivos de la práctica

* Conocer y practicar la programación orientada a objetos (con conceptos como clase,
  objeto, herencia, instanciacion).
* Usar varios modulos Python, importando funcionalidad creada en otros modulos.
* Conocer y seguir la guía de estilo de programacion recomendada para Python (ver PEP8).
* Utilizar el sistema de control de versiones git en GitLab.

## Ejercicio 1

Comprueba que puedes entrar en tu cuenta del [GitLab de la ETSIT](https://gitlab.etsit.urjc.es).

## Ejercicio 2

Con el navegador, dirígete al repositorio plantilla de esta práctica (ver al comienzo de este enunciado)
y realiza un "fork" del mismo en tu cuenta, de manera que consigas tener una
copia del repositorio en tu cuenta. Clona en tu ordenador
local el repositorio que acabas de crear, para poder editar los archivos localmente.
Trabaja a partir de ahora en ese repositorio, sincronizando
los cambios (commits) que vayas realizando según los ejercicios que se
detallan a continuación. Recuerda subir estos cambios a tu repositorio en
GitHub (normalmente, usando `git push`) para queden reflejados allí.

## Ejercicio 3

Investiga el archivo `calc.py`, en este repositorio.
Comprueba que en él se implementa una
calculadora sencilla que permite sumar y restar. El programa tiene las
siguientes características:

* Se invoca de la siguiente manera desde el terminal (shell):

```shell
python3 calc.py operando1 operación operando2
```

donde operación podrá ser suma o resta. Por ejemplo:

```shell
python3 calc.py 4 - 2
2.0
```

* Para tomar los operandos de los argumentos que se pasan al programa cuando se
ejecuta, se usa el módulo sys (`import sys`).
La lista `sys.argv` tiene los argumentos que se especificaron al ejecutar el programa.

* El programa comprueba que los parámetros correspondientes a operandos son numéricos
(pueden convertirse a `float`), y si no levanta una excepción que mostrará un error.
También se comprueba que el operador es `+` o `-`, y si no es así se muestra tambien
un mensaje de error.

* Tiene dos funciones (métodos): `add` para sumar y `sub` para restar.

* Muestra el resultado final en pantalla.

## Ejercicio 4

Copia el fichero 'calc.py' en tu clon local de tu repositorio para la práctica.
Añádelo a los ficheros que conoce git ( git add`), y no olvides hacer un commit
para registrar el cambio.

Ejecuta, desde esa copia en el clon local de tu repositorio para la práctica,
el programa `calc.py` en el terminal y desde PyCharm, con varios argumentos, incluyendo
algunos que fuercen que se muestren los mensajes de error.

Para ejecutarlo en PyCharm, tendrás que editar la
[configuración de ejecución del programa](https://www.jetbrains.com/help/pycharm/run-debug-configuration.html)
(puedes acceder a ella en el menú desplegable junto al icono de ejecución),
e indicar en ella los parámetros de ejecución como los escribirías en el terminal.

## Ejercicio 5

Crea en el archivo `calcoo.py` un programa Python que implemente la
misma funcionalidad (y se ejecute de la misma manera) que `calc.py`,
pero utilizando construcciones de orientación a objetos.

Para ello, crea una clase llamada `Calc`
(las mayúsculas son importantes), que tenga los métodos (funciones) `add` y `sub`
(igual que antes, devolverán el resultado, sin imprimir nada en pantalla)

El programa principal de `calcoo.py` aceptará los mismos parámetros
que `calc.py`, y funcionará de la misma manera. Pero ahora, en lugar de llamar
directamente a las funciones `add` y `sub`, deberá instanciar un objeto
de la clase `Calc`, y luego llamar a los métocos `add` y `sub` de este objeto.

Para entender cómo hacer este ejercicio, puedes estudiar primero el fichero
`ejemplo-oo.py`, que proporciona una clase Persona, otra clase Alumno que hereda
de ella, y en el programa principal instancia objetos de ambas clases, e invoca
métodos de ambos objetos. Te recomendamos también que estudies el
[capítulo "Classes & Iterators" de "Dive into Python3"](https://diveintopython3.net/iterators.html).
Por ahora sólo te hará falta la parte donde explica cómo se definen las clases, cómo se instancian,
y cómo funcionan las variables de la instancia.

Recuerda hacer un commit cuando termines el ejercicio (por lo menos), añadiendo previamente
el fichero `calcoo.py` a los ficheros conocidos por git.

## Ejercicio 6

Crea el archivo `calcoochild.py`, en el que escribirás una nueva clase,
`CalcChild`, que heradará de `Calc` (de `calcoo.py`), y añadirá dos métodos más
(`mul` y `div`) para multiplicar y dividir.
En el caso de `div` ha de capturar la excepción que saltará si el segundo operando
es cero, imprimiendo en ese caso el siguiente mensaje de error en pantalla:
"Division by zero is not allowed", y levantando (usando `raise`) una nueva
excepción `ValueError` con el mismo mensaje.

El programa principal hará lo mismo que el de `calcoo.py`, con las siguientes diferencias:

* Aceptará ahora las cuatro operaciones (+, -, x, /).
* En caso de que salte la excepción `ValueError` que levanta la función 'div'
se mostrará el mensaje "Error" en lugar del resultado de la operación.

Para hacer esto, no olvides, en `calcoochild.py` importar el módulo `calcoo.py`:

```py
import calcoo
```

Cuando tengas que referirte a `Calc` de `calcoo.py`, tendrás que hacerlo con la
notacion punto: `calcoo.Calc`.

Para entender mejor cómo gestionar las excepciones, explora el [apartado "Exceptions"
en "Dive into Python3"](https://diveintopython3.net/your-first-python-program.html#exceptions).

El programa `calcoochild.py` se ejecutará con los mismos argumentos que 
`calcoo.py` y `calc.py`, pero ahora aceptando también los operadores `x` (multiplicación)
y `/` (división). Por ejemplo:

```shell
python3 calcoochild.py 4 / 2
2.0
```

Recuerda hacer un commit cuando termines el ejercicio (por lo menos), añadiendo previamente
el fichero `calcoochild.py` a los ficheros conocidos por git.

## Ejercicio 7

Crea el archivo `calccount.py`, que tendrá una clase `Calc` que no heredará de ninguna otra,
e implementará toda la funcionalidad de `CalcChild` (funciones `add`, `sub`, `mul` y `div`).
Además, implementará un variable de instancia (`count`) que almacenará el número de veces que
se ha llamado a cualquiera de las funciones de la clase.

Este archivo no tendrá programa principal.

Para probar el módulo `calccount.py`, utiliza los tests que puedes encontrar en el
archivo `tests/test_calccount.py`. Ejecútalo desde PyCharm, pero tambien desde el terminal,
poniñendote en el directorio principal del repositorio, y escribiendo:

```shell
python3 -m unittest tests/test_calccount.py
```

Aprovecha para leer sobre tests en [Testing Your Code](https://docs.python-guide.org/writing/tests/),
de "The Hitchhiker's Guide to Python" (sobre todo la parte sobre `unittest`) y
la documentación de [`unittest`](https://docs.python.org/3/library/unittest.html).

Recuerda hacer un commit cuando termines el ejercicio (por lo menos), añadiendo previamente
el fichero `calccount.py` a los ficheros conocidos por git.


## Ejercicio 8

Crea el archivo `calccsv.py` que será llamado con un nombre de fichero como argumento:

```
python3 calccsv.py fichero
```

El fichero será un fichero de texto, con una o más líneas.
Cada línea estará en [formato CSV (comma-separated-value)](https://en.wikipedia.org/wiki/Comma-separated_values),
indicando una secuencia de operación y operandos, de la siguiente forma:

```
operación,operando1,operando2,operando3,...,operandoN
```

Para cada línea, `calccsv.py`  deberá tomar la operación con la que empieza cada
línea, y realizarla de manera secuencial con todos los operandos:
el primero con el segundo, el resultado de la operación con el tercero, y así sucesivamente.
imprimiendo en pantalla el resultado final de ejecutar cada línea.
Así, para las siguientes líneas:

```
+,1,2,3,4,5
-,31,6,4,3,2,1
x,1,3,5
/,300,10,2
```

el resultado que se imprimirá será 15 en todos los casos:

```
15.0
15.0
15.0
15.0
```

Si alguna de las operaciones es una división y el segundo operando es un cero,
deberá verse en pantalla el mensaje de error que muestra el método 'div',
y otro mensaje producido por el programa principal, que se mostrará en lugar del
resultado para esa línea:

```
Error: division by zero
```

Si alguna de las líneas no tiene el formato adecuado, el programa escribirá
"Bad format" y continuará procesando la siguiente línea.

El fichero `calccsv.py` no implementará los métodos ni las clases para realizar las operaciones,
sino que hará uso de la funcionalidad implementada en `calccount.py`, esto es,
de la funcionalidad implementada por la clase `Calc` de `calccount.py`.

El fichero `calccsv.py` proporcionará la funcionalidad indicada mediante una función,
`process_csv`, que aceptará como argumento un nombre de archivo, y escribirá en
pantalla el resultado de operar lo especificado en las líneas de ese fichero, como se
indica más arriba.

Para leer el fichero, utiliza el formato que hace uso de al la sentencia `with` de Python.
Puedes leer sobre esta sentencia en ["Closing files automatically" de "Dive into Python3](https://diveintopython3.net/files.html#with).

Para extraer la operación y los operandos de cada línea, considera usar la función [`split`](https://docs.python.org/3/library/stdtypes.html#str.split),
que se puede usar con cualquier string. Para eliminar el carácter fin de línea que habrá en cada
línea del fichero, si te hace falta, considera usar la función [`rstrip`](https://docs.python.org/3/library/stdtypes.html#str.rstrip),
que se puede usar también con cualquier string.

Aségurate de que tu programa funciona al menos con el fichero `operations.csv`, produciendo el siguiente resultado:

```
8.0
8.0
8.0
8.0
8.0
8.0
Division by zero is not allowed
Bad format
Bad format
Bad format
Bad format
8.0
```

Recuerda hacer un commit cuando termines el ejercicio (por lo menos), añadiendo previamente
el fichero `calccount.py` a los ficheros conocidos por git.

## Ejercicio 9

Crea el archivo `calccsv2.py` que será llamado con los mismos argumentos que
`calccsv.py`, y que funcionará de la misma manera, pero que usará el
[módulo `csv` de Python](https://docs.python.org/3.8/library/csv.html).

Pruébalo también con el fichero `operations.csv` (deberá producir el mismo resultado).

Recuerda hacer un commit cuando termines el ejercicio (por lo menos), añadiendo previamente
el fichero `calccsv2.py` a los ficheros conocidos por git.

## Ejercicio 10

Aunque el intérprete de Python admite ciertas libertades a la hora de
programar, los programadores de Python con la finalidad de mejorar
principalmente la legibilidad del código recomiendan seguir una guía
de estilo. Esta guía de estilo se encuentra en el [Python Enhancement
Proposal 8 (PEP 8)](https://www.python.org/dev/peps/pep-0008/), y contiene instrucciones sobre cómo situar los
espacios en blanco, cómo nombrar las variables, etc. Puedes leer también una
[explicación en español](https://ellibrodepython.com/python-pep8).

Hay programas para comprobar que un programa sigue correctamente PEP8, que además
te explican qué errores tienes si encuentran algunos. Para Python3 recomendamos
utilizar [pycodestyle](https://pycodestyle.pycqa.org)
(en Ubuntu, disponible si instalas el paquete `pycodestyle`), que puedes
aplicar a tus programas hasta que no detecte ningún error.
También puedes usar la [funcionalidad que proprociona PyCharm](https://www.jetbrains.com/help/pycharm/tutorial-code-quality-assistance-tips-and-tricks.html),
que tambien detecta los errores PEP8 si está adecuadamente configurado,
y te puede ayudar a reformatear el código para resolver esos errores.

Recuerda hacer un commit cuando termines el ejercicio (por lo menos), añadiendo previamente
el fichero `calcplusplus.py` a los ficheros conocidos por git.

No olvides sincronizar tus ficheros al repositorio de entrega (tu repositorio en
el GitLab de la ETSIT) para que podamos corregirlos (`git push`), y cuando hayas
terminado todo, comprobar que tu repositirio es público.

## ¿Qué se valora de esta la práctica?

Valoraremos de esta práctica sólo lo que esté en la rama principal de
tu repositorio, creado de la forma que 
hemos indicado (como fork del repositorio plantilla que os proporcionamos).
Por lo tanto, aségurate de que está en él todo lo que has realizado.

Además, ten en cuenta:
* Se valorará que haya realizado al menos haya ocho commits sobre la rama principal del repositorio.
* Se valorará que el código respete lo especificado en PEP8.
* Se valorará que estén todos los archivos que se piden en los ejercicios anteriores.
* Se valorar ́a que los programas se invoquen exactamente según se especifica, y que muestren
mensajes y errores correctamente según se indica en el enunciado de la práctica.
* Parte de la corrección será automática, así que asegúrate de que los nombres que utilizas
para archivos, clases, funciones, variables, etc. son los mismos que indica el enunciado.



## Ejercicio 11 (segundo periodo)

Una vez haya terminado el periodo de entrega de las prácticas, busca entre los forks
del repositorio original (del que hiciste tu fork al principio de esta práctica),
para localizar los repositorio de otros alumnos que la hayan entregado.
Elige uno, y haz un fork de él. Ojo, tendrás que usar un nombre distinto para el repositiorio,
porque ya tendrás el tuyo propio con ese nombre. Elije como nombre `repositorio-usuario`,
siendo `repositorio` el nombre del repositorio original, y `usuario` el identificador del
usuario del que estas haciendo el fork.

Crea un clon local del repositorio que acabas de crear (usando `git clone`),
y crea en él una rama nueva de desarrollo, en la que vas a implementar la funcionalidad
de este segundo periodo. Llama a esta rama `nueva`.

En esta rama tendrás todos los ficheros que entregó tu compañero. Por lo tanto, podrás
usar su módulo `calccount.py`. Crea uno nuevo, `calc[id].py`, que lo use
(`[id]` será tu identificador en el laboratorio: por ejemplo, si es `jgb`,
el fichero será `calcjgb.py`).
En este nuevo módulo crearás una nueva clase `CalcCalls` que heredará de
`calccount.Calc`, y que incluirá un nuevo método, `calls`, que devolverá el número
de veces que se ha llamado a funciones de la calculadora (`add`, `sub`, `mul`, `div`).
Recuerda que ese valor lo tendrás en la variable de instancia `count`, así que construir
este método será fácil.

Crea también un test en el directorio tests (`test/test_calc[id].py`) que compruebe
que si se llama a tres funciones de la calculadora el valor que devuelve este nuevo
método `calls` es 3.

Cuando termines, sincroniza tus ficheros en esta nueva rama
al repositorio de entrega (tu repositorio en
el GitLab de la ETSIT), usando `git push` cuando estás en la rama `nueva`.

A continuación, realiza un 'merge request' en el GitLab de la escuela, proponiendo
al repositorio del que clonaste (el de tu compañero) que acepte tu cambio.

Si recibes en tu repositorio cambios ('merge requests') de otros compañeros,
acepta todos los que puedas, si los nombres de los archivos que proponen añadir
son los correctos, y no hacen más cambios a tu repositorio que añadirlos.
